using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("forexAI EA DLL")]
[assembly: AssemblyDescription("expert advisor for mt4 with neural network data fuzz implemented")]
[assembly: AssemblyConfiguration("source")]
[assembly: AssemblyCompany("Axis9")]
[assembly: AssemblyProduct("forexAI mt4 ea DLL")]
[assembly: AssemblyCopyright("Copyright © Axis9  2018")]
[assembly: AssemblyTrademark("kilitary/deconf")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("ec9c4e35-86f8-479a-8138-8c45088667d4")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("0.1.25.10")]
[assembly: AssemblyFileVersion("0.1.25.10")]
